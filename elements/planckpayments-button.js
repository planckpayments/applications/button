(() => {
  var windowToElementMap = new Map(); //maps iframe windows to planckpayments-button elements for event handling

  var _myDOM = Symbol();
  var _wasConnected = Symbol();

  class PlanckPaymentButton extends HTMLElement {
    constructor() {
      super();
      this[_wasConnected] = false;
      this[_myDOM] = this.attachShadow({mode: 'closed'});
    }

    connectedCallback() {
      this[_wasConnected] = true;
      var value = this.getAttribute('value');
      var productID = this.getAttribute("product");
      var recipientID = this.getAttribute("recipient");

      var iframe = document.createElement('iframe');
      iframe.src = `CVAR_MAIN_URL/CVAR_BUTTON_PATH?value=${value}&recipient=${recipientID}&product=${productID}`;
      iframe.style = "width:100px; height:25px; border:none;";

      this[_myDOM].appendChild(iframe);

      windowToElementMap.set(iframe.contentWindow, this);
    }

    get onpayment() {
      var onpaymentAttr = this.getAttribute("onpayment");
      switch (typeof(onpaymentAttr)) {
        case 'string':
        return eval(onpaymentAttr);
        case 'function':
        return onpaymentAttr;
        default:
        throw "onpayment attribute must be a string or a function";
      }
    }

    set onpayment(v) {
      this.setAttribute("onpayment", v);
    }

    get value() {
      return this.getAttribute("value");
    }

    set value(v) {
      if (this[_wasConnected])
        throw "Cannot change value attribute of <planckpayments-button> element after it has been appended to the DOM";
      else {
        this.setAttribute("value", v);
      }
    }

    get product() {
      return this.getAttribute("product");
    }

    set product(v) {
      if (this[_wasConnected])
        throw "Cannot change product attribute of <planckpayments-button> element after it has been appended to the DOM";
      else
        this.setAttribute("product", v);
    }

    get recipient() {
      return this.getAttribute("recipient");
    }

    set recipient(v) {
      if (this[_wasConnected])
        throw "Cannot change recipient attribute of <planckpayments-button> element after it has been appended to the DOM";
      else
        this.setAttribute("recipient", v);
    }
  }

  customElements.define("planckpayments-button", PlanckPaymentButton);

  window.addEventListener('message', (e) => {
    if (e.origin === "CVAR_MAIN_URL" ) {
      var payButton = windowToElementMap.get(e.source);
      payButton.onpayment(e.data);
    }
  });
})();
