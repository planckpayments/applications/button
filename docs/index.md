---
title: PlanckPayments button docs
---

# What it is

The planckpayments button allows website owners to incorporate single-click payments for any bank or PSP adhering to the Planck Payments standard.

If the client pressed the payment button and payment succeeds, a digitally signed receipt is created that can be used to verify the validity of the payment and can function as authorization to serve content.


# Status

This is a mock of a demo. No real payments are made.
The API is expected to change frequently. 


# How to use

## Frontend

In your HTML, include our payments button script:

```HTML
<script src="CVAR_MAIN_URL/planckpayments-button.js"></script>
```

You can then add the planckpayments button as an html element:

```HTML
<planckpayments-button value=10 product="fancy-shoes" recipient="my bank account" onpayment="notifyPayment"></planckpayments-button>
```

The following attributes are available:

- `value`: the ammount to be paid, in 1/10ths of a eurocent. Must be a natural number between 1 and 30k.
- `product`: an identifier for the purchased product for your own use.
- `recipient`: where to send payment to. Exact format tbd.
- `onpayment`: a handler to e.g. display the purchased content. Takes a digitally signed receipt as per Plack Payments specification. Can be a function or the name of a function.


## Serving content

It is possible to check the validity of content on the frontend, but doing so makes it easy to circumvent your paywall. You may as well not verify the receipt it at all. It is up to you whether this is an acceptable tradeoff.


## Minimal usage example

```HTML
<!DOCTYPE html5>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Planck Payments button example usage</title>
    
    <script src="CVAR_MAIN_URL/planckpayments-button.js"></script>
  </head>

  <body>
    <planckpayments-button value=10 product="funny-cats-video-38" recipient="my bank account" onpayment="notifyPayment"></planckpayments-button>
  </body>

  <script>
    function notifyPayment(receipt) {
      alert("thank you for your purchase");
    }
  </script>
</html>
```
