PP=sed
PP_FLAGS=

ELEMENTS_DIR=elements
DOCS_DIR=docs

BUILD_DIR=build
DOCS_DIR_BUILD=$(BUILD_DIR)/$(DOCS_DIR)

PUBLIC_DIR=public
DOCS_DIR_OUT=$(PUBLIC_DIR)/$(DOCS_DIR)

ELEMENTS_IN=$(wildcard $(ELEMENTS_DIR)/*)
ELEMENTS_OUT=$(patsubst $(ELEMENTS_DIR)/%, $(PUBLIC_DIR)/%, $(ELEMENTS_IN))

DOCS_IN=$(wildcard $(DOCS_DIR)/*.md)
DOCS_BUILD=$(patsubst $(DOCS_DIR)/%.md, $(DOCS_DIR_BUILD)/%.md, $(DOCS_IN))
DOCS_OUT=$(patsubst $(DOCS_DIR)/%.md, $(DOCS_DIR_OUT)/%.html, $(DOCS_IN))

REDIRECT=_redirects

MAIN_URL=https://button.demo.planckpayments.com
BUTTON_PATH=button

.phony: local demo production clean

clean:
	rm -rf $(PUBLIC_DIR)

local: MAIN_URL = http://localhost:8000
local: BUTTON_PATH = button.html


REPLACEMENTS=-e 's,CVAR_MAIN_URL,$(MAIN_URL),g' -e 's,CVAR_BUTTON_PATH,$(BUTTON_PATH),g'

local: demo
demo: production $(PUBLIC_DIR)/example.html
production: $(ELEMENTS_OUT) $(DOCS_OUT) $(PUBLIC_DIR)/$(REDIRECT)

$(PUBLIC_DIR):
	mkdir $@

$(BUILD_DIR):
	mkdir $@

$(DOCS_DIR_BUILD): | $(BUILD_DIR)
	mkdir $@

$(DOCS_DIR_OUT): | $(PUBLIC_DIR)
	mkdir $@

$(PUBLIC_DIR)/%: $(ELEMENTS_DIR)/% | $(PUBLIC_DIR)
	$(PP) $(PP_FLAGS) $(REPLACEMENTS) $< > $@

$(PUBLIC_DIR)/$(REDIRECT): $(REDIRECT) | $(PUBLIC_DIR)
	cp $(REDIRECT) $(PUBLIC_DIR)/$(REDIRECT)

$(PUBLIC_DIR)/example.html: example.html | $(PUBLIC_DIR)
	$(PP) $(PP_FLAGS) $(REPLACEMENTS) $< > $@

$(DOCS_DIR_OUT)/%.html: $(DOCS_DIR_BUILD)/%.md | $(DOCS_DIR_OUT)
	pandoc --standalone -f markdown -t html -c /pandoc.css $< -o $@
	
$(DOCS_DIR_BUILD)/%.md: $(DOCS_DIR)/%.md | $(DOCS_DIR_BUILD)
	$(PP) $(PP_FLAGS) $(REPLACEMENTS) $< > $@

